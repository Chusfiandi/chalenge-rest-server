<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// user routes
$route['user/register'] = 'users/users/register';
$route['user/login'] = 'users/users/login';


// customer routes
$route['customer/register'] = 'users/customer/customer';
$route['customer/login'] = 'users/customer/login';
$route['api/customer'] = 'users/customer/customer';
$route['api/customer/(:num)/delete']['DELETE'] = 'users/customer/customer/$1';

/* transaksi routes */
// post
$route['api/transaksi/add'] = 'api/transaksi/createTransaksi';
// GET cart
$route['api/transaksi'] = 'api/transaksi/transaksi';
// checkout
$route['api/transaksi/checkout'] = 'api/transaksi/checkout';
// checkout B
$route['api/transaksi/checkoutA'] = 'api/transaksi/checkoutB';
// get histori
$route['api/transaksi/histori'] = 'api/transaksi/transaksiStatus';
// get All histori
$route['api/transaksi/allhistori'] = 'api/transaksi/transaksiAll';


/*Produk routes */
// get
$route['api/product'] = 'api/products/produk';
// post
$route['api/product/add'] = 'api/products/addProduct';
// delete
$route['api/product/(:num)/delete']['DELETE'] = 'api/products/deleteProduct/$1';
// PUT
// $route['api/product/update']['put'] = 'api/products/updateProduct';
$route['api/product/update'] = 'api/products/updateProduct';

/*Hadiah routes */
// get
$route['api/give-away'] = 'api/award/award';
// post
$route['api/give-away/add'] = 'api/award/addaward';
// delete
$route['api/give-away/(:num)/delete']['DELETE'] = 'api/award/deleteaward/$1';
// PUT
$route['api/give-away/update'] = 'api/award/updateaward';

/*Point routes */
// add
$route['customer/point/add'] = 'customer/point/addpoint';
// point
$route['customer/point'] = 'customer/awardcus/poin';
// hadiah customer
$route['customer/award'] = 'customer/awardcus/awardcus';
// add hadiah customer
$route['customer/award/add'] = 'customer/awardcus/add';