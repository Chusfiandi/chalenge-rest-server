<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Products extends REST_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('products_model', 'productsM');
    }



    public function produk_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id');
            // var_dump($id);
            // die;
            $product = $this->productsM->getProduct($id);
            if ($product) {
                $this->response([
                    'status' => TRUE,
                    'data' => $product
                ]);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ]);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }

    // create Product
    public function addProduct_post()
    {
        header("Access-Control-Allow-Origin: *");
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);
            $this->form_validation->set_rules('name_product', 'Name Product', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[50]|numeric');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $nama = $_FILES['image']['name'];
                $nameImage = rand(1000000, 100000000) . $nama;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['upload_path'] = './assets/img';
                $config['file_name'] = $nameImage;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'file failed upload'
                    ]);
                } else {
                    $diskon = $this->input->post('diskon');
                    $price = $this->input->post('price');
                    if ($diskon) {
                        $harga = $price - (($diskon / 100) * $price);
                        $insert_data = [
                            'name_product' => $this->input->post('name_product', TRUE),
                            'image' => $nameImage,
                            'price' => $harga,
                            'diskon' => $this->input->post('diskon', TRUE),
                            'created_at' => date('D F Y'),
                            'update_at' => date('D F Y')
                        ];
                    } else {
                        $insert_data = [
                            'name_product' => $this->input->post('name_product', TRUE),
                            'image' => $nameImage,
                            'price' => $this->input->post('price', TRUE),
                            'diskon' => 0,
                            'created_at' => date('D F Y'),
                            'update_at' => date('D F Y')
                        ];
                    }
                    $output = $this->productsM->add_products($insert_data);
                    if ($output == true) {
                        $message =  [
                            'status' => true,
                            'message' => " add product successful "
                        ];
                        $this->response($message, REST_Controller::HTTP_OK);
                    } else {
                        $message =  [
                            'status' => false,
                            'message' => " add product failed "
                        ];
                        $this->response($message, REST_Controller::HTTP_OK);
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }

    // Delete Product
    public function deleteProduct_delete($id)
    {
        header("Access-Control-Allow-Origin: *");

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->security->xss_clean($id);

            if (empty($id) and !is_numeric($id)) {
                $this->response(['status' => FALSE, 'message' => "invalid Product ID"], REST_Controller::HTTP_OK);
            } else {
                $product = $this->db->get_where('tbl_produk', ['id' => $id])->row();
                if ($this->productsM->delete_product($id) == true) {
                    unlink(FCPATH . 'assets/img/' . $product->image);
                    $message =  [
                        'status' => true,
                        'message' => " Product deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " Product not deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }

    public function updateProduct_put()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $this->form_validation->set_rules('name_product', 'Name Product', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[50]|numeric');
            $this->form_validation->set_rules('id', 'id', 'trim|required|max_length[50]|numeric');
            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $id = $this->input->post('id');
                // $id_user = $is_valid_token['data']->id;
                $product = $this->db->get_where('tbl_produk', ['id' => $id])->row();
                $jumlah_produk = $this->db->get_where('tbl_produk', ['id' => $id])->num_rows();
                $oldImage = $product->image;
                if (empty($_FILES)) {
                    if ($jumlah_produk != 0) {
                        $diskon = $this->input->post('diskon');
                        if ($diskon) {
                            $price = $this->input->post('price');
                            $harga = $price - (($diskon / 100) * $price);
                            $data = [
                                "name_product" => $this->input->post("name_product"),
                                "price" => $harga,
                                "diskon" => $diskon,
                                "update_at" => date('D F Y')
                            ];
                        } else {
                            $data = [
                                "name_product" => $this->input->post("name_product"),
                                "price" => $this->input->post("price"),
                                "diskon" => 0,
                                "update_at" => date('D F Y')
                            ];
                        }
                        $this->db->update('tbl_produk', $data, ['id' => $id]);

                        $this->response([
                            'status' => true,
                            'message' => 'Update success'
                        ], 200);
                    }
                } else {
                    $newImage = rand(1000000, 100000000) . $_FILES['image']['name'];
                    $config['upload_path']          = './assets/img';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    // $new_name = time() . $_FILES["userfiles"]['name'];
                    $config['file_name'] = $newImage;
                    // var_dump($newImage);
                    // die;
                    if ($jumlah_produk != 0) {
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('image')) {
                            $this->response([
                                'status' => false,
                                'message' => 'File Gagal di Update'
                            ], 404);
                        } else {
                            unlink(FCPATH . "/assets/img/" . $oldImage);
                            $diskon = $this->input->post('diskon');
                            if ($diskon) {
                                $price = $this->input->post('price');
                                $harga = $price - (($diskon / 100) * $price);
                                $data = [
                                    "name_product" => $this->input->post("name_product"),
                                    "price" => $harga,
                                    "diskon" => $diskon,
                                    "image" => $newImage,
                                    "update_at" => date('D F Y')
                                ];
                            } else {
                                $data = [
                                    "name_product" => $this->input->post("name_product"),
                                    "price" => $this->input->post("price"),
                                    "diskon" => $this->input->post("diskon"),
                                    "image" => $newImage,
                                    "update_at" => date('D F Y')
                                ];
                            }
                            $this->db->update('tbl_produk', $data, ['id' => $id]);
                            $this->response([
                                'status' => true,
                                'message' => 'Update success'
                            ], 200);
                        }
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }
}