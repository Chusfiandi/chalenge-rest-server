<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Transaksi extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaksi_model', 'transaksiM');
    }
    // get transaksi cus status = 0
    public function transaksi_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id_customer = $this->get('id_customer');

            $status = 0;

            $transaksi = $this->transaksiM->getTransaksi($id_customer, $status);
            if ($transaksi) {
                $this->response([
                    'status' => true,
                    'data' => $transaksi,
                    'message' => 'Produk ditemukan'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'data' => $transaksi,
                    'message' => 'Produk belum ada diKeranjang'
                ], 200);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }

    // get all transaksi
    public function transaksiAll_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id_customer = $this->get('id_customer');

            $status = 0;

            $transaksi = $this->transaksiM->getAllTransaksi();
            if ($transaksi) {
                $this->response([
                    'status' => true,
                    'data' => $transaksi,
                    'message' => 'Produk ditemukan'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'data' => $transaksi,
                    'message' => 'Produk tidak ditemukan'
                ], 404);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // create transaksi
    public function createTransaksi_post()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $this->load->library('Authorization_Token');

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);

            $this->form_validation->set_rules('id_product', 'id product', 'trim|required|numeric');
            $this->form_validation->set_rules('qty', 'qty', 'trim|required|max_length[10]|numeric');
            $this->form_validation->set_rules('id_customer', 'id customer', 'trim|required|numeric');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $idProduct = $this->post('id_product');
                $qty = $this->post('qty');
                $this->load->model('products_model');
                $product = $this->products_model->getProduct($idProduct);
                $insert_data = [
                    'id_customer' => $this->input->post('id_customer', TRUE),
                    'id_product' => $this->input->post('id_product', TRUE),
                    'name_product' => $product->name_product,
                    'price_product' => $product->price,
                    'diskon' => $product->diskon,
                    'qty' => $this->input->post('qty', TRUE),
                    'total' => $product->price * $qty,
                    'status' => 0,
                    'created_at' => date('D F Y')
                ];
                $output = $this->transaksiM->create_transaksi($insert_data);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " transaction successful "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " transaction failed "
                    ];
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // function checkout_post()
    // {

    //     $this->form_validation->set_rules('id_customer', 'id product', 'trim|required|numeric');
    //     $this->form_validation->set_rules('id_transaksi', 'id transaksi', 'trim|required|max_length[10]|numeric');
    //     if ($this->form_validation->run() == FALSE) {
    //         $message =  [
    //             'status' => false,
    //             'error' => $this->form_validation->error_array(),
    //             'message' => validation_errors()
    //         ];
    //         $this->response($message, REST_Controller::HTTP_NOT_FOUND);
    //     } else {
    //         $id_customer = $this->input->post('id_customer');
    //         $id_transaksi = $this->input->post('id_transaksi');
    //         if ($this->transaksiM->cekTransaksi($id_customer) == true) {
    //         } else {
    //             $this->response([
    //                 'status' => false,
    //                 'message' => 'Tidak ada barang di keranjang'
    //             ], 200);
    //         }

    //         if ($this->transaksiM->checkout($id_customer, $id_transaksi) == true) {

    //             if ($this->transaksiM->cekidPointUser($id_customer) == 0) {
    //                 $dataP = [
    //                     'id_customer' => $id_customer,
    //                     'point' => 5
    //                 ];

    //                 if ($this->transaksiM->tambahPoint($dataP) == true) {
    //                     $this->response([
    //                         'status' => true,
    //                         'data' => $dataP,
    //                         'message' => 'Transaksi masuk, selamat anda mendapat 5 point pertama'
    //                     ], 200);
    //                 } else {
    //                     $this->response([
    //                         'status' => false,
    //                         'message' => 'Data transaksi gagal dimasukkan'
    //                     ], 404);
    //                 }
    //             } else {
    //                 $dataP = $this->transaksiM->cekPoint($id_customer);
    //                 $point = $dataP['point'];
    //                 $pointT = $point + 5;

    //                 if ($this->transaksiM->ubahPoint($id_customer, $pointT) == true) {
    //                     $this->response([
    //                         'status' => true,
    //                         'point' => $pointT,
    //                         'message' => 'Transaksi berhasil, point anda bertambah 5'
    //                     ], 200);
    //                 } else {
    //                     $this->response([
    //                         'status' => false,
    //                         'message' => 'Data transaksi gagal dimasukkan'
    //                     ], 404);
    //                 }
    //             }
    //         } else {
    //             $this->response([
    //                 'status' => false,
    //                 'message' => 'Data transaksi gagal dimasukkan'
    //             ], 404);
    //         }
    //     }
    // }
    function isipoin2($id_customer)
    {
        if ($this->transaksiM->cekidPointUser($id_customer) == 0) {
            $dataP = [
                'id_customer' => $id_customer,
                'point' => 2
            ];

            if ($this->transaksiM->tambahPoint($dataP) == true) {
                $this->response([
                    'status' => true,
                    'data' => $dataP,
                    'message' => 'Transaksi masuk, selamat anda mendapat 2 point pertama'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Data transaksi gagal dimasukkan'
                ], 404);
            }
        } else {
            $dataP = $this->transaksiM->cekPoint($id_customer);
            $point = $dataP['point'];
            $pointT = $point + 2;

            if ($this->transaksiM->ubahPoint($id_customer, $pointT) == true) {
                $this->response([
                    'status' => true,
                    'point' => $pointT,
                    'message' => 'Transaksi berhasil, point anda bertambah 2'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Data transaksi gagal dimasukkan'
                ], 404);
            }
        }
    }

    function isipoin5($id_customer)
    {
        if ($this->transaksiM->cekidPointUser($id_customer) == 0) {
            $dataP = [
                'id_customer' => $id_customer,
                'point' => 5
            ];

            if ($this->transaksiM->tambahPoint($dataP) == true) {
                $this->response([
                    'status' => true,
                    'data' => $dataP,
                    'message' => 'Transaksi masuk, selamat anda mendapat 2 point pertama'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Data transaksi gagal dimasukkan'
                ], 404);
            }
        } else {
            $dataP = $this->transaksiM->cekPoint($id_customer);
            $point = $dataP['point'];
            $pointT = $point + 5;

            if ($this->transaksiM->ubahPoint($id_customer, $pointT) == true) {
                $this->response([
                    'status' => true,
                    'point' => $pointT,
                    'message' => 'Transaksi berhasil, point anda bertambah 5'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Data transaksi gagal dimasukkan'
                ], 404);
            }
        }
    }

    public function checkoutB_post()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        $id_customer = $this->input->post('id_customer');
        $id_transaksi = $this->input->post('id_transaksi');
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $this->form_validation->set_rules('id_transaksi', 'id transaksi', 'trim|required|numeric');
            $this->form_validation->set_rules('id_customer', 'id customer', 'trim|required|max_length[10]|numeric');
            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                if ($this->transaksiM->checkout($id_customer, $id_transaksi) == true) {
                    $transaksi = $this->transaksiM->getTotal($id_customer, $id_transaksi);
                    if ($transaksi == true) {
                        if ($transaksi->total < 100000) {
                            if ($this->transaksiM->checkout($id_customer, $id_transaksi) == true) {
                                $this->response([
                                    'status' => true,
                                    'message' => 'Transaksi berhasil'
                                ], 200);
                            } else {
                                $this->response([
                                    'status' => false,
                                    'message' => 'Data transaksi gagal dimasukan'
                                ], 200);
                            }
                        } elseif ($transaksi->total > 100000 && $transaksi->total < 200000) {
                            $this->isipoin2($id_customer);
                        } else {
                            $this->isipoin5($id_customer);
                        }
                    } else {
                        $this->response([
                            'status' => false,
                            'message' => 'Produk dikeranjang Kosong'
                        ], 200);
                    }
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data transaksi gagal dimasukkan'
                    ], 404);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    public function transaksiStatus_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id_customer = $this->get('id_customer');

            $status = 1;

            $transaksi = $this->transaksiM->getTransaksi($id_customer, $status);
            if ($transaksi) {
                $this->response([
                    'status' => true,
                    'data' => $transaksi,
                    'message' => 'Produk ditemukan'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'data' => $transaksi,
                    'message' => 'Produk tidak ditemukan'
                ], 200);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }
}