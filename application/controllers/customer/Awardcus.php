<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Awardcus extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cus_model');
    }
    public function poin_get()
    {
        header("Access-Control-Allow-Origin: *");
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id_cus = $this->get('id_customer');
            $query = $this->cus_model->poin($id_cus);
            if ($query) {
                $this->response([
                    'status' => true,
                    'data' => $query
                ], 200);
            } else {
                $this->response([
                    'status' => 'false',
                    'message' => 'Tidak ada Poin silahkan transaksi'
                ], 200);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }

    public function awardcus_get()
    {
        header("Access-Control-Allow-Origin: *");
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id_customer');
            $query = $this->cus_model->getAward($id);
            if ($query) {
                $this->response([
                    'status' => TRUE,
                    'data' => $query
                ], 200);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ], 200);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }
    public function add_post()
    {
        header("Access-Control-Allow-Origin: *");
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);

            $this->form_validation->set_rules('id_customer', 'id customer', 'trim|required|numeric');
            $this->form_validation->set_rules('id_hadiah', 'id hadiah', 'trim|required|numeric');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $id_hadiah = $this->post('id_hadiah');

                $hadiah = $this->cus_model->cekHadiah($id_hadiah);
                $id_customer = $this->post('id_customer');
                $data = [
                    'id_user'     => $id_customer,
                    'id_hadiah'   => $hadiah->id,
                    'nama_hadiah' => $hadiah->gift_name,
                    'point'       => $hadiah->poin
                ];

                if ($this->cus_model->cekPoin($id_customer) == 0) {
                    $this->response([
                        'status' => false,
                        'message' => 'Data Hadiah gagal dimasukkan'
                    ], 200);
                } else {
                    $dataP = $this->cus_model->cekPoint($id_customer);
                    $point = $dataP['point'];
                    if ($point < $data['point']) {
                        $this->response([
                            'status' => false,
                            'message' => 'Point tidak cukup'
                        ], 200);
                    } else {
                        $sisaPoin = $point - $data['point'];
                        if ($this->cus_model->ubahPoint($id_customer, $sisaPoin) == true) {
                            $isi = ['id_customer' => $id_customer, 'id_hadiah' => $hadiah->id];
                            $a = $this->cus_model->exchange($isi);
                            $this->response([
                                'status' => true,
                                'pointSisa' => $sisaPoin,
                                'message' => 'Hadiah dapat ditukarkan'
                            ], 200);
                        } else {
                            $this->response([
                                'status' => false,
                                'message' => 'Hadiah tidak dapat ditukarkan'
                            ], 200);
                        }
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_OK);
        }
    }
}