<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Customer extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model', 'userM');
    }

    public function customer_post()
    {
        header("Access-Control-Allow-Origin: *");
        $_POST = $this->security->xss_clean($_POST);

        $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]|alpha');
        $this->form_validation->set_rules(
            'username',
            'Username',
            'trim|required|max_length[50]|alpha_numeric|is_unique[tbl_customer.username]',
            array('is_unique' => 'This %s already exists please enter another username')
        );
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('name', 'name', 'trim|required|max_length[100]');

        if ($this->form_validation->run() == FALSE) {
            $message =  [
                'status' => false,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors()
            ];
            $this->response($message, 200);
        } else {
            $insert_data = [
                'name' => $this->input->post('name', TRUE),
                'username' => $this->input->post('username', TRUE),
                'password' =>  password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'level' => 'customer',
                'created_at' => date('D F Y')
            ];

            $output = $this->userM->insert_cus($insert_data);

            if ($output > 0 and !empty($output)) {
                $message =  [
                    'status' => true,
                    'message' => "customer registration successful"
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $message =  [
                    'status' => false,
                    'message' => "Not register your account"
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
        }
    }

    public function login_post()
    {
        header("Access-Control-Allow-Origin: *");

        $_POST = $this->security->xss_clean($_POST);

        $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[100]');

        if ($this->form_validation->run() == FALSE) {
            $message =  [
                'status' => false,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors()
            ];
            $this->response($message, REST_Controller::HTTP_OK);
        } else {
            $output = $this->userM->cus_login($this->input->post('username'), $this->input->post('password'));
            if (!empty($output) and $output != FALSE) {
                $return_data = [
                    'id_customer' => $output->id,
                    'name' => $output->name,
                    'username' => $output->username,
                    'created_at' => $output->created_at,
                    'level' => $output->level
                ];

                $message =  [
                    'status' => true,
                    'data' => $return_data,
                    'message' => "Customer login successful"
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $message =  [
                    'status' => false,
                    'message' => "Invalid Username or Password"
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            }
        }
    }
    // Delete customer
    public function customer_delete($id)
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $this->load->library('Authorization_Token');

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->security->xss_clean($id);

            if (empty($id) and !is_numeric($id)) {
                $this->response(['status' => FALSE, 'message' => "invalid customer ID"], REST_Controller::HTTP_NOT_FOUND);
            } else {
                $delete_customer = [
                    'id' => $id,
                    'id_user' => $is_valid_token['data']->id,
                ];
                $output = $this->userM->delete_customer($delete_customer);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " customer deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " customer not deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    // tampilkan customer
    public function customer_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id');
            // var_dump($id);
            // die;
            $id_user = $is_valid_token['data']->id;
            $customer = $this->userM->getcustomer($id, $id_user);
            if ($customer) {
                $this->response([
                    'status' => TRUE,
                    'data' => $customer
                ]);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ]);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}