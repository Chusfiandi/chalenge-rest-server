<?php defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'third_party/php-jwt/JWT.php';
require_once APPPATH . 'third_party/php-jwt/BeforeValidException.php';
require_once APPPATH . 'third_party/php-jwt/ExpiredException.php';
require_once APPPATH . 'third_party/php-jwt/SignatureInvalidException.php';

use \Firebase\JWT\JWT;

class Authorization_Token
{

    protected $token_key;


    protected $token_algorithm;


    protected $token_header = ['authorization', 'Authorization'];


    protected $token_expire_time = 259200;


    public function __construct()
    {
        $this->CI = &get_instance();


        $this->CI->load->config('jwt');


        $this->token_key        = $this->CI->config->item('jwt_key');
        $this->token_algorithm  = $this->CI->config->item('jwt_algorithm');
    }


    public function generateToken($data)
    {
        try {
            return JWT::encode($data, $this->token_key, $this->token_algorithm);
        } catch (Exception $e) {
            return 'Message: ' . $e->getMessage();
        }
    }


    public function validateToken()
    {

        $headers = $this->CI->input->request_headers();


        $token_data = $this->tokenIsExist($headers);
        if ($token_data['status'] === TRUE) {
            try {

                try {
                    $token_decode = JWT::decode($headers[$token_data['key']], $this->token_key, array($this->token_algorithm));
                } catch (Exception $e) {
                    return ['status' => FALSE, 'message' => $e->getMessage()];
                }

                if (!empty($token_decode) and is_object($token_decode)) {

                    if (empty($token_decode->id) or !is_numeric($token_decode->id)) {
                        return ['status' => FALSE, 'message' => 'User ID Not Define!'];
                    } else if (empty($token_decode->time or !is_numeric($token_decode->time))) {

                        return ['status' => FALSE, 'message' => 'Token Time Not Define!'];
                    } else {

                        $time_difference = strtotime('now') - $token_decode->time;
                        if ($time_difference >= $this->token_expire_time) {
                            return ['status' => FALSE, 'message' => 'Token Time Expire.'];
                        } else {

                            return ['status' => TRUE, 'data' => $token_decode];
                        }
                    }
                } else {
                    return ['status' => FALSE, 'message' => 'Forbidden'];
                }
            } catch (Exception $e) {
                return ['status' => FALSE, 'message' => $e->getMessage()];
            }
        } else {

            return ['status' => FALSE, 'message' => $token_data['message']];
        }
    }


    public function validateTokenPost()
    {
        if (isset($_POST['token'])) {
            $token = $this->CI->input->post('token', TRUE);
            if (!empty($token) and is_string($token) and !is_array($token)) {
                try {
                    /**
                     * Token Decode
                     */
                    try {
                        $token_decode = JWT::decode($token, $this->token_key, array($this->token_algorithm));
                    } catch (Exception $e) {
                        return ['status' => FALSE, 'message' => $e->getMessage()];
                    }

                    if (!empty($token_decode) and is_object($token_decode)) {
                        // cek User ID (tidak and numeric)
                        if (empty($token_decode->id) or !is_numeric($token_decode->id)) {
                            return ['status' => FALSE, 'message' => 'User ID Not Define!'];

                            // cek Token Time
                        } else if (empty($token_decode->time or !is_numeric($token_decode->time))) {

                            return ['status' => FALSE, 'message' => 'Token Time Not Define!'];
                        } else {
                            /**
                             * cek Token Time Valid 
                             */
                            $time_difference = strtotime('now') - $token_decode->time;
                            if ($time_difference >= $this->token_expire_time) {
                                return ['status' => FALSE, 'message' => 'Token Time Expire.'];
                            } else {
                                /**
                                 * All Validation False Return Data
                                 */
                                return ['status' => TRUE, 'data' => $token_decode];
                            }
                        }
                    } else {
                        return ['status' => FALSE, 'message' => 'Forbidden'];
                    }
                } catch (Exception $e) {
                    return ['status' => FALSE, 'message' => $e->getMessage()];
                }
            } else {
                return ['status' => FALSE, 'message' => 'Token is not defined.'];
            }
        } else {
            return ['status' => FALSE, 'message' => 'Token is not defined.'];
        }
    }


    public function tokenIsExist($headers)
    {
        if (!empty($headers) and is_array($headers)) {
            foreach ($this->token_header as $key) {
                if (array_key_exists($key, $headers) and !empty($key))
                    return ['status' => TRUE, 'key' => $key];
            }
        }
        return ['status' => FALSE, 'message' => 'Token is not defined.'];
    }


    public function userData()
    {

        $headers = $this->CI->input->request_headers();


        $token_data = $this->tokenIsExist($headers);
        if ($token_data['status'] === TRUE) {
            try {

                try {
                    $token_decode = JWT::decode($headers[$token_data['key']], $this->token_key, array($this->token_algorithm));
                } catch (Exception $e) {
                    return ['status' => FALSE, 'message' => $e->getMessage()];
                }

                if (!empty($token_decode) and is_object($token_decode)) {
                    return $token_decode;
                } else {
                    return ['status' => FALSE, 'message' => 'Forbidden'];
                }
            } catch (Exception $e) {
                return ['status' => FALSE, 'message' => $e->getMessage()];
            }
        } else {
            // Authorization Header Not Found!
            return ['status' => FALSE, 'message' => $token_data['message']];
        }
    }
}