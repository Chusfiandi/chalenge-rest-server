<?php

class Cus_model extends CI_model
{
    public function getAward($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_exchange a');
        $this->db->join('tbl_customer b', 'b.id=a.id_customer', 'left');
        $this->db->join('tbl_hadiah c', 'c.id=a.id_hadiah', 'left');
        $this->db->where('b.id', $id);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function ubahPoint($id_cus, $sisaPoin)
    {
        $this->db->where('id_customer', $id_cus);
        $data = $this->db->update('tbl_poin', ['point' => $sisaPoin]);
        return $data;
    }
    public function exchange($isi)
    {
        $isi = $this->db->insert('tbl_exchange', $isi);
        return $isi;
    }
    public function cekPoin($id_cus)
    {
        $data = $this->db->get_where('tbl_poin', ['id_customer' => $id_cus])->num_rows();
        return $data;
    }
    public function poin($id_cus)
    {
        $data = $this->db->get_where('tbl_poin', ['id_customer' => $id_cus])->row();
        return $data;
    }
    function cekHadiah($id_hadiah)
    {
        $data = $this->db->get_where('tbl_hadiah', ['id' => $id_hadiah]);
        return $data->row();
    }
    function cekPoint($id_customer)
    {
        $data = $this->db->get_where('tbl_poin', ['id_customer' => $id_customer]);
        return $data->row_array();
    }
}