<?php

class Products_model extends CI_model
{
    protected $products_table = 'tbl_produk';

    public function getProduct($id)
    {

        if ($id === null) {
            $product = $this->db->get($this->products_table)->result();
        } else {
            $product = $this->db->get_where($this->products_table, ['id' => $id])->row();
        }
        return $product;
    }

    public function add_products(array $data)
    {
        $data = $this->db->insert($this->products_table, $data);
        return $data;
    }

    public function delete_product($id)
    {
        $data = $this->db->delete($this->products_table, ['id' => $id]);
        return $data;
    }
}