<?php

class Transaksi_model extends CI_model
{
    private $tbltransaksi = "tbl_transaksi";
    private $tblpoint = "tbl_poin";
    public function getTransaksi($id_customer, $status)
    {
        $this->db->where('id_customer', $id_customer);
        $this->db->where('status', $status);
        $transaksi = $this->db->get($this->tbltransaksi)->result();
        return $transaksi;
    }
    public function getAllTransaksi()
    {
        $transaksi = $this->db->get($this->tbltransaksi)->result();
        return $transaksi;
    }
    public function create_transaksi($insert_data)
    {
        $data = $this->db->insert($this->tbltransaksi, $insert_data);
        return $data;
    }
    function cekTransaksi($id_customer)
    {
        $status = 0;
        $array = ['id_customer' => $id_customer, 'status' => $status];
        $data = $this->db->get_where($this->tbltransaksi, $array);
        return $data->num_rows();
    }
    function checkout($id_customer, $id_transaksi)
    {
        $this->db->where('id_customer', $id_customer);
        $this->db->where('id', $id_transaksi);
        $data = $this->db->update($this->tbltransaksi, ['status' => 1]);
        return $data;
    }

    function cekIdPointUser($id_customer)
    {
        $data = $this->db->get_where($this->tblpoint, ['id_customer' => $id_customer]);
        return $data->num_rows();
    }

    function tambahPoint($dataP)
    {
        $data = $this->db->insert($this->tblpoint, $dataP);
        return $data;
    }

    function cekPoint($id_customer)
    {
        $data = $this->db->get_where($this->tblpoint, ['id_customer' => $id_customer]);
        return $data->row_array();
    }

    function ubahPoint($id_customer, $pointT)
    {
        $this->db->where('id_customer', $id_customer);
        $data = $this->db->update($this->tblpoint, ['point' => $pointT]);
        return $data;
    }
    public function getTotal($id_customer, $id_transaksi)
    {
        $this->db->where('id', $id_transaksi);
        $this->db->where('id_customer', $id_customer);
        $transaksi = $this->db->get($this->tbltransaksi)->row();
        return $transaksi;
    }
}