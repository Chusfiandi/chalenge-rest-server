<?php

class Users_model extends CI_model
{
    protected $user_table = 'tbl_user';
    protected $tbl_cus = 'tbl_customer';

    // insert user
    public function insert_user($user_data)
    {
        $this->db->insert($this->user_table, $user_data);
        return $this->db->insert_id();
    }

    // insert customer
    public function insert_cus($cus_data)
    {
        $this->db->insert($this->tbl_cus, $cus_data);
        return $this->db->insert_id();
    }

    public function user_login($username, $password)
    {
        $this->db->where('email', $username);
        $this->db->or_where('username', $username);
        $q = $this->db->get($this->user_table);

        if ($q->num_rows()) {
            $user_pass = $q->row('password');
            if (password_verify($password, $user_pass)) {
                return $q->row();
            }
            return FALSE;
        } else {
            return FALSE;
        }
    }
    public function cus_login($username, $password)
    {
        $this->db->where('username', $username);
        $q = $this->db->get($this->tbl_cus);

        if ($q->num_rows()) {
            $user_pass = $q->row('password');
            if (password_verify($password, $user_pass)) {
                return $q->row();
            }
            return FALSE;
        } else {
            return FALSE;
        }
    }
    public function delete_customer(array $data)
    {
        $query = $this->db->get_where($this->tbl_cus, $data);
        if ($this->db->affected_rows() > 0) {
            $this->db->delete($this->tbl_cus, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
            return false;
        }
        return false;
    }
    public function getcustomer($id, $id_user)
    {
        $this->db->where('id_user', $id_user);
        if ($id === null) {
            $product = $this->db->get($this->tbl_cus)->result();
        } else {
            $product = $this->db->get_where($this->tbl_cus, ['id' => $id])->row();
        }
        return $product;
    }
}