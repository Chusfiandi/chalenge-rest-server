-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Agu 2020 pada 08.29
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pointsale`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('admin','customer') NOT NULL,
  `created_at` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `name`, `username`, `password`, `level`, `created_at`) VALUES
(9, 'customer', 'customer', '$2y$10$xTpWid0X2ajueGh5PxNEYeDS0IdI9KaDwnntql1pxeekPrm5ELNqC', 'customer', 'Mon August 2020'),
(10, 'coba', 'coba', '$2y$10$nQc8Zm64b.1jp.4W1ut04OyxBjVA.wqWN3JERjz3wZOmd0GBDKhge', 'customer', 'Tue August 2020'),
(12, 'chusfiandi', 'chusfiandi', '$2y$10$JHmFbKuM54VWW3u5eUBTNumzqF2E5lAD3n6ZolUzS4vbr0ZkYfo1O', 'customer', 'Tue August 2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_exchange`
--

CREATE TABLE `tbl_exchange` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_hadiah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_exchange`
--

INSERT INTO `tbl_exchange` (`id`, `id_customer`, `id_hadiah`) VALUES
(1, 9, 6),
(2, 9, 2),
(3, 9, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hadiah`
--

CREATE TABLE `tbl_hadiah` (
  `id` int(11) NOT NULL,
  `gift_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `poin` int(11) NOT NULL,
  `created_at` text NOT NULL,
  `update_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_hadiah`
--

INSERT INTO `tbl_hadiah` (`id`, `gift_name`, `image`, `poin`, `created_at`, `update_at`) VALUES
(1, 'sepeda polygon', '81459909download.jpg', 100, '1597205935', '1597205935'),
(2, 'Tv LG', '95255173tv.jpg', 67, '1597207873', '1597724341'),
(6, 'Masker', '22432165masker-pintar-mi-copy_ratio-16x9.jpg', 15, '1597637112', '1597724366');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_poin`
--

CREATE TABLE `tbl_poin` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_poin`
--

INSERT INTO `tbl_poin` (`id`, `id_customer`, `point`) VALUES
(4, 12, 352),
(5, 9, 77),
(6, 10, 20);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `id` int(11) NOT NULL,
  `name_product` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `created_at` varchar(125) NOT NULL,
  `update_at` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_produk`
--

INSERT INTO `tbl_produk` (`id`, `name_product`, `image`, `price`, `diskon`, `created_at`, `update_at`) VALUES
(1, 'Sepeda Polygon', '83622373download.jpg', 2250000, 10, 'Wed August 2020', 'Tue August 2020'),
(4, 'Masker', '20042699masker-pintar-mi-copy_ratio-16x9.jpg', 12000, 20, 'Tue August 2020', 'Tue August 2020'),
(5, 'sepatu Vans', '647286936473105_88640ad3-f325-403c-a863-0c24a9139401_1280_1280.jpg', 186000, 7, 'Tue August 2020', 'Tue August 2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `name_product` varchar(255) NOT NULL,
  `price_product` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id`, `id_customer`, `id_product`, `name_product`, `price_product`, `diskon`, `qty`, `total`, `status`, `created_at`) VALUES
(1, 12, 3, 'Sepeda Pasific', 1600000, 20, 2, 3200000, 1, 'Sun August 2020'),
(2, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 1, 2460000, 1, 'Sun August 2020'),
(3, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 1, 2460000, 1, 'Sun August 2020'),
(4, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 2, 4920000, 1, 'Sun August 2020'),
(5, 12, 3, 'Sepeda Pasific', 1600000, 20, 2, 3200000, 1, 'Sun August 2020'),
(6, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 2, 4920000, 1, 'Sun August 2020'),
(7, 12, 10, 'Sepeda Kuat sekali', 1750000, 30, 1, 1750000, 1, 'Sun August 2020'),
(8, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 3, 7380000, 1, 'Sun August 2020'),
(9, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 2, 4920000, 1, 'Sun August 2020'),
(10, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 1, 2460000, 1, 'Sun August 2020'),
(11, 12, 3, 'Sepeda Pasific', 1600000, 20, 1, 1600000, 1, 'Sun August 2020'),
(12, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 2, 4920000, 1, 'Sun August 2020'),
(13, 12, 3, 'Sepeda Pasific', 1600000, 20, 4, 6400000, 1, 'Sun August 2020'),
(14, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 3, 7380000, 1, 'Sun August 2020'),
(15, 12, 3, 'Sepeda Pasific', 1600000, 20, 3, 4800000, 1, 'Sun August 2020'),
(16, 12, 4, 'Sepeda Lipat Pro', 2460000, 18, 2, 4920000, 1, 'Sun August 2020'),
(17, 12, 3, 'Sepeda Pasific', 1600000, 20, 15, 24000000, 1, 'Sun August 2020'),
(18, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(19, 9, 1, 'Sepeda Polygon', 180000, 10, 4, 720000, 1, 'Mon August 2020'),
(20, 9, 1, 'Sepeda Polygon', 180000, 10, 3, 540000, 1, 'Mon August 2020'),
(21, 9, 1, 'Sepeda Polygon', 180000, 10, 3, 540000, 1, 'Mon August 2020'),
(22, 9, 1, 'Sepeda Polygon', 180000, 10, 1, 180000, 1, 'Mon August 2020'),
(23, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(24, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(25, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(26, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(27, 9, 1, 'Sepeda Polygon', 180000, 10, 4, 720000, 1, 'Mon August 2020'),
(28, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(29, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(30, 9, 1, 'Sepeda Polygon', 180000, 10, 19, 3420000, 1, 'Mon August 2020'),
(31, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(32, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(33, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(34, 9, 1, 'Sepeda Polygon', 180000, 10, 2, 360000, 1, 'Mon August 2020'),
(35, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 2, 1120000, 1, 'Mon August 2020'),
(36, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 2, 1120000, 1, 'Mon August 2020'),
(37, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 5, 2800000, 1, 'Mon August 2020'),
(38, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 1, 560000, 1, 'Mon August 2020'),
(39, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 1, 560000, 1, 'Mon August 2020'),
(40, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 3, 1680000, 1, 'Mon August 2020'),
(41, 9, 3, 'Sepeda Pasific Pro', 560000, 20, 1, 560000, 0, 'Mon August 2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `created_at` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `email`, `username`, `password`, `level`, `created_at`) VALUES
(9, 'chusfiandi', 'fiandichus3@gmail.com', 'chusfiandi', '$2y$10$AjYkpbNuVRNIY2lBK5UxouA0OG/vVh0AYZM5xZGlTi6/AsiXX2hsm', 'admin', 'Mon August 2020'),
(10, 'admin', 'admin@gmail.com', 'admin', '$2y$10$SgfiWUCVPpiXV/stihLdleuWsEudsnv0SGYH9Pf9TyxwvQS5Y3pDC', 'admin', 'Wed August 2020'),
(11, 'tes', 'tes@gmail.com', 'tes', '$2y$10$w/J5aWADCwdFB/YqOGqn3OUAjcSa2twMnETeoRaQvb19WrIxJpAr2', 'admin', 'Wed August 2020');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_exchange`
--
ALTER TABLE `tbl_exchange`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hadiah`
--
ALTER TABLE `tbl_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_poin`
--
ALTER TABLE `tbl_poin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tbl_exchange`
--
ALTER TABLE `tbl_exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_hadiah`
--
ALTER TABLE `tbl_hadiah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_poin`
--
ALTER TABLE `tbl_poin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
